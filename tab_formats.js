
(function($) {
/**
 * This was strickly a Proof of Concept Module. To try an improve the way the filters are display
 */
 Drupal.behaviors.tabFormats = {
   attach: function(context, settings) {
     $('.tab-formats', context).once(function(i, val) {
       var options = $('option', $(this));
       var ul = $('<ul>').addClass('tab-formats-ul primary');
       options.each(function (i, opt) {
         opt = $(opt);
         var a = $('<a>').attr('href', "#"+opt.val()).text(opt.text()).attr("data-click", opt.val());
         a.click(function() {
           $('select.tab-formats').val($(this).attr('data-click'));
           $('select.tab-formats').change();
           $('.filter-tabs-li').removeClass('active');
           $(this).parent().addClass('active');
           return false;
         });
         li = $('<li>').addClass('filter-tabs-li');
         li.append(a);
         ul.append(li);
       });
       $('.form-item-body-und-0-value').before(ul);
       $('.filter-wrapper').hide();
     });
   }
 };
})(jQuery);
